using ExadelTestProjectTests;
using System;
using Xunit;

namespace ExadelProjectTests
{
    public class Tests
    {
        [Theory]
        [InlineData("1 + 1", 2)]
        [InlineData("8/16", 0.5)]
        [InlineData("3 -(-1)", 4)]
        [InlineData("2 + -2", 0)]
        [InlineData("10- 2- -5", 13)]
        [InlineData("(((10)))", 10)]
        [InlineData("3 * 5", 15)]
        [InlineData("-7 * -(6 / 3)", 14)]
        [InlineData("1-1", 0)]
        [InlineData("1 -1", 0)]
        [InlineData("1- 1", 0)]
        [InlineData("1 - 1", 0)]
        [InlineData("1- -1", 2)]
        [InlineData("1 - -1", 2)]
        [InlineData("1--1", 2)]
        [InlineData("6 + -(4)", 2)]
        [InlineData("6 + -( -4)", 10)]
        [InlineData("(2 / (2 + 3.33) * 4) - -6", 7.50093808630394)]
        public void Run(string expression, double expectation)
        {
            PostFixSolution p = new PostFixSolution();
            var response = p.Calculate(expression);

            Assert.Equal(expectation, response);
        }
    }
}
