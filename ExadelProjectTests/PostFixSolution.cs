﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ExadelTestProjectTests
{
    public class PostFixSolution
    {
        public double Calculate(string expression)
        {
            var arr = CustomSplit(expression);
            var postFix = ConvertToPostFix(arr);
            var answer = GetAnswer(postFix);

            return answer;
        }

        private double GetAnswer(List<string> expression)
        {
            var symbols = new List<string> { "*", "/", "+", "_" };
            Stack<double> s = new Stack<double>();
            foreach (var item in expression)
            {
                if (symbols.Contains(item))
                {
                    var secondItem = s.Pop();
                    var firstItem = s.Pop();
                    var op = item;
                    var response = DoOperation(firstItem, secondItem, op);
                    s.Push(response);
                    continue;
                }

                s.Push(Convert.ToDouble(item));

            }

            return s.Pop();
        }

        private double DoOperation(double firstItem, double secondItem, string op)
        {
            if (op == "+")
            {
                return firstItem + secondItem;
            }

            if (op == "_")
            {
                return firstItem - secondItem;
            }

            if (op == "*")
            {
                return firstItem * secondItem;
            }

            if (op == "/")
            {
                return firstItem / secondItem;
            }

            return 0;
        }


        private int Position(string op)
        {
            if (op == "+" || op == "_")
            {
                return 1;
            }
            else if (op == "*" || op == "/")
            {
                return 2;
            }
            else
            {
                return 0;
            }
        }

        private List<string> ConvertToPostFix(List<string> expression)
        {
            var resposnseList = new List<string>();
            Stack<string> s = new Stack<string>();
            s.Push("&"); // some symbol 
            var symbols = new List<string> { "(", ")", "*", "/", "+", "_" };
            foreach (var item in expression)
            {
                if (!symbols.Contains(item))
                {
                    resposnseList.Add(item);
                }
                else if (item == "(")
                {
                    s.Push("(");
                }
                else if (item == ")")
                {
                    while (s.Peek() != "&" && s.Peek() != "(")
                    {
                        resposnseList.Add(s.Pop());
                    }
                    s.Pop();
                }
                else
                {
                    if (Position(item) > Position(s.Peek()))
                        s.Push(item);
                    else
                    {
                        while (s.Peek() != "&" && Position(item) <= Position(s.Peek()))
                        {
                            resposnseList.Add(s.Pop());
                        }
                        s.Push(item);
                    }
                }
            }

            while (s.Peek() != "&")
            {
                resposnseList.Add(s.Pop());
            }

            return resposnseList;
        }
        private List<string> CustomSplit(string expression)
        {
            //  Stack<string> q = new Stack<string>();
            List<string> q = new List<string>();
            var symbols = new List<string> { "(", ")", "*", "/", "+", "_" };
            var correctString = expression.Replace(" ", "");
            foreach (var item in correctString)
            {
                var symbol = item.ToString();
                if (q.Count == 0)
                {
                    q.Add(symbol);
                    continue;
                }

                if (!symbols.Contains(symbol))
                {
                    string prev = q.Last();
                    if (symbol == "-")
                    {
                        MinusLogic(prev, q);
                        continue;
                    }


                    if (!symbols.Contains(prev))
                    {
                        prev += symbol;
                        q[q.Count - 1] = prev;
                    }
                    else
                    {
                        q.Add(symbol);
                    }
                }
                else
                {
                    if (symbol == "(")
                    {
                        ParenthesesLogic(q.Last(), q);
                    }
                    q.Add(symbol);
                }

            }

            return q;
        }

        private void ParenthesesLogic(string prev, List<string> q)
        {
            if (prev == "-")
            {
                q[q.Count - 1] = "-1";
                q.Add("*");
            }
        }

        private void MinusLogic(string prev, List<string> q)
        {
            var symbols = new List<string> { "(", ")", "*", "/", "+", "_" };
            if (prev == "-")
            {
                q[q.Count - 1] = "+";
                return;
            }

            if (prev == "+")
            {
                q[q.Count - 1] = "_";
                return;
            }

            if (prev == ")")
            {
                q.Add("_");
                return;
            }

            if (!symbols.Contains(prev))
            {
                q.Add("_");
                return;
            }

            q.Add("-");
        }
    }
}
